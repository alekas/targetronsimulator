﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rssdp;
using WebSocketSharp.Server;
using WebSocketSharp;

namespace TargetSimulator
{
    public class Target : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        private Button btnHit;
        private TextBox txtIpAddress;
        private TextBox txtSSDPURL;
        private TextBox txtGuid;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        string ipAddress;
        string macAddress;
        int port;
        int ssdpDescriptionPort;
        TcpListener listener;
        WebSocketServer websocketServer;
        private SsdpDevicePublisher _publisher;
        private string guid = Guid.NewGuid().ToString();


        delegate void SetColorCallback(Color color);

        public delegate void HitHandler(object sender, TargetEventArgs e);
        public event HitHandler OnHit;
       
        public Target()
        {
            this.ipAddress = GetLocalIPAddress();
            this.macAddress = "";
            listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();

            listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            ssdpDescriptionPort = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();


            this.websocketServer = new WebSocketServer(null, port);
            websocketServer.AddWebSocketService<TargetWebSocketBehavior>("/", () => new TargetWebSocketBehavior(this));
            websocketServer.Start();
            InitializeComponent();

            this.createCircularRegion();
            txtIpAddress.Text = GetLocalIPAddress() + ":" + port;
            txtSSDPURL.Text = getDescriptionUrl();
            txtGuid.Text = guid;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.onMouseDown);

            this.Connected = false;

            WebServer webserver = new WebServer(new string[] {getHostName()}, sendDescription);
            webserver.Run();
        }

        private string getHostName()
        {
            return string.Format("http://{0}:{1}/", GetLocalIPAddress(), ssdpDescriptionPort);
        }

        private string getDescriptionUrl()
        {
            return getHostName() + "description.xml";
        }

        public string sendDescription(HttpListenerRequest request)
        {
            return DeviceDefinition.ToDescriptionDocument();
        }

        public SsdpRootDevice DeviceDefinition
        {
            get
            {
                return new SsdpRootDevice()
                {
                    CacheLifetime = TimeSpan.FromMinutes(30),
                    Location = new Uri(getDescriptionUrl()),
                    DeviceTypeNamespace = "iventuresolutions",
                    DeviceType = "target",
                    FriendlyName = "iVenture Targetron",
                    Manufacturer = "Team Hercules",
                    ModelName = "Targetron Simulator",
                    Uuid = guid,
                    CustomProperties = {new SsdpDeviceProperty() {Name = "websocketPort", Value = port.ToString()} }
                };
            }
        }

        public bool Connected { get; set; }

        private void createCircularRegion()
        {

            GraphicsPath path = new GraphicsPath();
            path.AddEllipse(0, 0, this.ClientSize.Width, this.ClientSize.Height);
            Region region = new Region(path);
            this.Region = region;
            this.FormBorderStyle = FormBorderStyle.None;
        }

        private void onClick(object sender, EventArgs e)
        {
        }

        private void onMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public void setColor(int color)
        {
            Color temp = Color.FromArgb(color);
            Color result = Color.FromArgb(temp.R, temp.G, temp.B);

            setColor(result);
        }
        public void setColor(Color color) {
            if (this.InvokeRequired)
            {
                SetColorCallback d = new SetColorCallback(setColor);
                this.Invoke(d, new object[] {color});
            }
            else
            {
                this.BackColor = color;
            }
        }

        private void InitializeComponent()
        {
            this.btnHit = new System.Windows.Forms.Button();
            this.txtIpAddress = new System.Windows.Forms.TextBox();
            this.txtSSDPURL = new System.Windows.Forms.TextBox();
            this.txtGuid = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnHit
            // 
            this.btnHit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnHit.Location = new System.Drawing.Point(54, 103);
            this.btnHit.Name = "btnHit";
            this.btnHit.Size = new System.Drawing.Size(182, 23);
            this.btnHit.TabIndex = 0;
            this.btnHit.Text = "Hit";
            this.btnHit.UseVisualStyleBackColor = true;
            this.btnHit.Click += new System.EventHandler(this.btnHit_Click);
            // 
            // txtIpAddress
            // 
            this.txtIpAddress.Location = new System.Drawing.Point(54, 129);
            this.txtIpAddress.Name = "txtIpAddress";
            this.txtIpAddress.Size = new System.Drawing.Size(182, 20);
            this.txtIpAddress.TabIndex = 1;
            // 
            // txtSSDPURL
            // 
            this.txtSSDPURL.Location = new System.Drawing.Point(54, 155);
            this.txtSSDPURL.Name = "txtSSDPURL";
            this.txtSSDPURL.Size = new System.Drawing.Size(182, 20);
            this.txtSSDPURL.TabIndex = 2;
            // 
            // txtGuid
            // 
            this.txtGuid.Location = new System.Drawing.Point(54, 181);
            this.txtGuid.Name = "txtGuid";
            this.txtGuid.Size = new System.Drawing.Size(182, 20);
            this.txtGuid.TabIndex = 3;
            // 
            // Target
            // 
            this.ClientSize = new System.Drawing.Size(300, 300);
            this.Controls.Add(this.txtGuid);
            this.Controls.Add(this.txtSSDPURL);
            this.Controls.Add(this.btnHit);
            this.Controls.Add(this.txtIpAddress);
            this.Name = "Target";
            this.Load += new System.EventHandler(this.Target_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Target_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnHit_Click(object sender, EventArgs e)
        {
            if (OnHit == null)
            {
                return;
            }
            TargetEventArgs args = new TargetEventArgs();
            OnHit(this, args);
        }

        private void Target_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Target_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Connected ? Color.Blue : Color.Black, 3);
            e.Graphics.DrawEllipse(pen, new Rectangle(1, 1, this.Width - 2, this.Height- 2));
            pen.Dispose();
        }
    }

    public class TargetWebSocketBehavior : WebSocketBehavior
    {
        private Target target;

        public TargetWebSocketBehavior(Target target)
        {
            this.target = target;
            target.OnHit += HandleHit;
        }

        private void HandleHit(object sender, TargetEventArgs e)
        {
            this.Send("HIT");
        }

        protected override Task OnMessage(MessageEventArgs e)
        {

            string command = e.Text.ReadToEnd();
            Console.WriteLine(command);
            if (command.StartsWith("#"))
            {
                Color color = System.Drawing.ColorTranslator.FromHtml(command);
                target.setColor(color);
            }
            return base.OnMessage(e);
        }

        protected override Task OnOpen()
        {
            Console.WriteLine("Opened");
            target.Connected = true;
            return base.OnOpen();
        }

        protected override Task OnClose(CloseEventArgs e)
        {
            target.Connected = false;
            return base.OnClose(e);
        }
    }

    public class TargetEventArgs : EventArgs
    {

    }
}
