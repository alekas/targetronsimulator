﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rssdp;

namespace TargetSimulator
{
    public partial class Form1 : Form
    {

        private SsdpDevicePublisher _publisher;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _publisher = new SsdpDevicePublisher();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Target target = new Target();
            _publisher.AddDevice(target.DeviceDefinition);
            target.setColor(0xff0000);
            target.Show();
        }
    }
}
